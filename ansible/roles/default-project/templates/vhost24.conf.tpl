# Default Apache virtualhost template

<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    DocumentRoot {{ www_root }}/{{ doc_root }}
    ServerName {{ hostname }}

    <Directory {{ www_root }}/{{ doc_root }}>
        AllowOverride All
        Options -Indexes +FollowSymLinks
        Require all granted
    </Directory>

    <FilesMatch \.php$>
        SetHandler php{{ php_version }}-fcgi
    </FilesMatch>

</VirtualHost>

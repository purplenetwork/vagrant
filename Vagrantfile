def which(cmd)
    exts = ENV['PATHEXT'] ? ENV['PATHEXT'].split(';') : ['']
    ENV['PATH'].split(File::PATH_SEPARATOR).each do |path|
        exts.each { |ext|
            exe = File.join(path, "#{cmd}#{ext}")
            return exe if File.executable? exe
        }
    end
    return nil
end

module OS
  def OS.windows?
    (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
  end

  def OS.mac?
   (/darwin/ =~ RUBY_PLATFORM) != nil
  end

  def OS.unix?
    !OS.windows?
  end

  def OS.linux?
    OS.unix? and not OS.mac?
  end
end

require 'yaml'

CONF = YAML.load(File.open('vagrant.yml', File::RDONLY).read)

# Define ansible folder location
if not File.exists?(File.join(Dir.pwd, 'vendor/purplenetwork/vagrant/ansible/ansible.cfg')) then
    ANSIBLE_FOLDER  = '/var/www/vagrant/ansible'
    ANSIBLE_CONFIG  = '/var/www/vagrant/ansible/ansible.cfg'
else
    ANSIBLE_FOLDER  = '/var/www/vagrant/vendor/purplenetwork/vagrant/ansible'
    ANSIBLE_CONFIG  = '/var/www/vagrant/vendor/purplenetwork/vagrant/ansible/ansible.cfg'
end

Vagrant.configure("2") do |config|

    # Configure the box to use
    config.vm.box       = 'ubuntu/trusty64'

    # Configure the name of the machine
    config.vm.define CONF["hostname"]

    # Configure the hostname
    config.vm.hostname = CONF["hostname"]

    # Configure the network interfaces
    config.vm.network :private_network, ip:    "10.10.10.10"
    config.vm.network :forwarded_port,  guest: 80,    host: 8080
    config.vm.network :forwarded_port,  guest: 8081,  host: 8081
    config.vm.network :forwarded_port,  guest: 3306,  host: 3310
    config.vm.network :forwarded_port,  guest: 27017, host: 27017
    config.vm.network :forwarded_port,  guest: 35279, host: 35279

    # SSH
    config.ssh.port = 2222
    config.ssh.forward_agent = true
    config.vm.network :forwarded_port, guest: 22, host: 2222, id: 'ssh'

    # Git
    if File.exists?(File.join(Dir.home, '.gitconfig')) then
        config.vm.provision :file do |file|
            file.source      = '~/.gitconfig'
            file.destination = '~/.gitconfig'
        end
    end

    # Composer
    if File.exists?(File.join(Dir.home, '.composer/auth.json')) then
        config.vm.provision :file do |file|
            file.source      = '~/.composer/auth.json'
            file.destination = '~/.composer/auth.json'
        end
    end

    # Copy params file to group_vars
    config.vm.provision :file do |file|
        file.source      = 'vagrant.yml'
        file.destination = ANSIBLE_FOLDER + '/group_vars/all/vagrant.yml'
    end

    # Configure shared folders
    if OS.linux? then
        config.vm.synced_folder ".", "/var/www/vagrant", type: "nfs", nfs_udp: false, nfs_version: "4", mount_options: ["fsc", "ac", "rw"]
    end

    if OS.mac? then
        config.vm.synced_folder ".", "/var/www/vagrant", type: "nfs", nfs_udp: false, nfs_version: "3", mount_options: ["fsc", "ac", "rw"]
    end

    if OS.windows? then
        config.vm.synced_folder ".", "/var/www/vagrant", type: "smb", mount_options: ["sec=ntlmssp", "mfsymlinks"]
    end

    # Configure VirtualBox environment
    config.vm.provider "virtualbox" do |v|
        v.name = CONF["hostname"]
        v.memory = CONF["vm_memory"]
        v.cpus = CONF["vm_cpus"]
    end

    # Ansible provisioning
    config.vm.provision 'shell',
    env: {
            "ANSIBLE_CONFIG" => ANSIBLE_CONFIG,
            "ANSIBLE_FOLDER" => ANSIBLE_FOLDER
        },
    inline:
        "
         apt-get -y install software-properties-common
         apt-add-repository -y ppa:ansible/ansible
         apt-get -y update
         apt-get -y install ansible
         cd $ANSIBLE_FOLDER
         ansible-galaxy install -r requirements.yml --force
         ansible-playbook -i '10.10.10.10,' -c local -u vagrant $ANSIBLE_FOLDER/provision.yml --extra-vars \"env=vagrant private_interface=10.10.10.10 ansible_ssh_user=vagrant\"
        "

    # Info
    config.vm.provision 'shell',
    run: "always",
    env: {
            "HOSTNAME" => CONF['hostname']
        },
    inline:
        "
         echo '***************************************************'
         echo '***************************************************'
         echo 'Time to go to work NOW!!'
         echo '***************************************************'
         echo 'Set in your hosts file:'
         echo '10.10.10.10  '$HOSTNAME
         echo '***************************************************'
        "
end

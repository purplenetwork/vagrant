- download library with composer:
```bash
composer require purplenetwork/vagrant --ignore-platform-reqs --dev --no-scripts
```

- create a file named  ```Vagrantfile``` in the root of the project:
```bash
external = File.read 'vendor/purplenetwork/vagrant/Vagrantfile'
eval external

Vagrant.configure("2") do |config|
    # Custom commands (clear cache, execute specific tasks, etc)
    config.vm.provision 'shell',
    run: "always",
    inline:
        "
        echo `pwd`
        "
end
```

- create a file named  ```vagrant.yml``` in the root of the project (see vagrant-dist.yml for details):
```bash
hostname: 'test-project.local' # No spaces and no special characters, allowed only '-' and '.'

vm_memory: 2048
vm_cpus: 2

db_name: 'test-db'
db_user: 'test'
db_password: 'test'

php_version: 5.6 # or 7.0 or 7.1

doc_root: 'web' # Optional. Don't set if not needed
```

- then run:
```bash
vagrant up # to start
vagrant halt | suspend # to halt or suspend
vagrant up --provision # to re-run provision, when you change something in vagrant.yml or update the vagrant library
vagrant destroy # to destroy all
```